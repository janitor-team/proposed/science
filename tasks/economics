Task: Economics
Install: false
Description: Debian Science Economics packages
 This metapackage will install Debian Science packages useful for economics and
 econometrics. It includes user-friendly programs for simulating and estimating
 macro-economic and micro-economic models. It also provides computing
 environments which can solve a wide range of problems typically encountered in
 economic research. These environments provide functionalities similar to those
 of popular non-free systems (such as MATLAB, Mathematica, Stata or SAS).

Recommends: dynare

Recommends: gretl

Recommends: octave-econometrics

Recommends: r-base

Recommends: r-cran-urca

Recommends: r-cran-bayesm

Recommends: r-cran-foreign

Recommends: r-cran-hmisc

Recommends: r-cran-tseries

Recommends: r-cran-mfilter

Recommends: r-cran-lme4

Recommends: r-cran-mcmcpack

Recommends: r-cran-fimport

Recommends: r-cran-fnonlinear

Recommends: r-cran-funitroots

Recommends: r-cran-gmm

Recommends: r-cran-wdi

Recommends: r-cran-pwt

Recommends: r-cran-pwt8

Recommends: r-cran-pwt9

Recommends: r-cran-rsdmx

Recommends: r-cran-isocodes

Recommends: r-cran-dynlm

Recommends: r-cran-aer

Recommends: r-cran-plm

Recommends: r-cran-rdbnomics

Recommends: python3-statsmodels

Recommends: dolo
Homepage: https://github.com/albop/dolo
License: BSD
Pkg-Description: Economic modelling in Python
 Dolo is a tool to assist researchers in solving several types of Dynamic
 Stochastic General Equilibrium (DSGE) models, using either local of global
 approximation methods.
 .
 Users are can separate the definition of their models from the solution
 algorithm. A simple syntax is provided in YAML files to define variables and
 equations of several types. This syntax integrates the specification of
 occasionally binding constraints (a.k.a. complementarity conditions)
 .
 The user can then implement his own preferred solution method using one of the
 provided tools (various types of interpolation, solvers, ...) or use one of
 the already implemented procedures.
 .
 Dolo is written in Python and so are his solution routines. If you prefer or
 need to use another language for the solution, you can always use dolo as a
 preprocessor. In that case, dolo will just translate the model file into a
 numerical file usable by your software. Currently, Octave/MATLAB and Julia are
 supported.

Recommends: recs
Homepage: http://www.recs-solver.org/
License: Expat (mostly)
Pkg-Description: MATLAB solver for DSGE models
 RECS is a MATLAB solver for dynamic, stochastic, rational expectations
 equilibrium models. RECS stands for "Rational Expectations Complementarity
 Solver". This name emphasizes that RECS has been developed specifically to
 solve models that include complementarity equations, also known as models with
 occasionally binding constraints.
 .
 RECS is designed to solve small-scale nonlinear and complementarity models,
 but not large-scale models. For solving large-scale problems, but without
 complementarity equations, see Dynare or similar toolboxes.

Recommends: python3-quantecon
Homepage: https://quantecon.org/quantecon-py
License: BSD-3-clause
Pkg-Description: high performance, open source Python code library for economics
 QuantEcon provides a high performance, open source Python code library
 for economics.

Recommends: r-cran-ecdat
Homepage: https://CRAN.R-project.org/package=Ecdat
License: GPL-2+
Pkg-Description: R data sets for econometrics
 Contains data sets that can be used to replicate a large set of published
 papers.

Recommends: r-other-bmr
Homepage: http://www.kthohr.com/bmr
License: GPL-2+
Pkg-Description: bayesian Macroeconometrics in R
 BMR (Bayesian Macroeconometrics in R) is a collection of R and C++ routines
 for estimating Bayesian Vector Autoregressive (BVAR) and Dynamic Stochastic
 General Equilibrium (DSGE) models in the R statistical environment. Features
 .
 For BVARs, BMR supports:
 * normal-inverse-Wishart prior,
 * Minnesota prior, and
 * Mattias Villani's steady-state prior.
 The BMR package can also estimate BVARs with time-varying parameters, as well
 as classical (non-Bayesian) VARs.
 .
 For DSGE models, the package can:
 * solve models using either Harald Uhlig's method of undetermined coefficients
   or Chris Sims' canonical decomposition;
 * estimate models using MCMC by means of a Kalman filter or the Chandrasekhar
   recursions;
 * and estimate a hybrid DSGE-VAR model.

Recommends: minsky
Homepage: http://sourceforge.net/p/minsky/
License: GPL-3
Pkg-Description: system dynamics program with additional features for economics
 Minsky is one of a family of ``system dynamics'' computer programs. These
 programs allow a dynamic model to be constructed, not by writing mathematical
 equations or numerous lines of computer code, but by laying out a model of a
 system in a flowchart, which can then simulate the system. These programs are
 now the main tool used by engineers to design complex products, ranging from
 small electrical components right up to passenger jets.
 .
 What does Minsky provide that other system dynamics programs don't boils down
 to one feature: The Godley Table that enables a dynamic model of financial
 flows to be derived from a table that is very similar to the accountant's
 double-entry bookkeeping table. Hence Minsky is very well suited for simulating
 Stock-Flow Consistent (SFC) models.

Recommends: r-cran-vars
Homepage: https://CRAN.R-project.org/package=vars
License: GPL-2+
Pkg-Description: VAR, SVAR and SVEC Models in R
 This packages implements vector autoregressive-, structural vector
 autoregressive- and structural vector error correction models in R. In
 addition to the three cornerstone functions VAR(), SVAR() and SVEC() for
 estimating such models, functions for diagnostic testing, estimation of a
 restricted models, prediction, causality analysis, impulse response analysis
 and forecast error variance decomposition are provided too. It is further
 possible to convert vector error correction models into their level VAR
 representation.

Recommends: r-other-gecon
Homepage: http://gecon.r-forge.r-project.org/
License: BSD
Pkg-Description: solver for large scale dynamic general equilibrium models
 gEcon allows users to describe their models in terms of optimisation problems
 of agents. Given optimisation problems, constraints and identities, gEcon
 derives the first order conditions, steady state equations, and linearisation
 matrices automatically. Numerical solvers can be then employed to determine
 the steady state and approximate equilibrium laws of motion around it.

Recommends: netlogo
Homepage: http://ccl.northwestern.edu/netlogo/
License: GPL-2+
Pkg-Description: multi-agent programmable modeling environment
 NetLogo is a programmable modeling environment for simulating natural and
 social phenomena. It was authored by Uri Wilensky in 1999 and has been in
 continuous development ever since at the Center for Connected Learning and
 Computer-Based Modeling.
 .
 NetLogo is particularly well suited for modeling complex systems developing
 over time. Modelers can give instructions to hundreds or thousands of "agents"
 all operating independently. This makes it possible to explore the connection
 between the micro-level behavior of individuals and the macro-level patterns
 that emerge from their interaction.

Recommends: r-cran-pdfetch
Homepage: https://CRAN.R-project.org/package=pdfetch
License: GPL-2+
Pkg-Description: Fetch Economic and Financial Time Series Data from Public Sources
 Download economic and financial time series from public sources, including the
 St Louis Fed's FRED system, Yahoo Finance, the US Bureau of Labor Statistics,
 the US Energy Information Administration, the World Bank, Eurostat, the
 European Central Bank, the Bank of England, the UK's Office of National
 Statistics, Deutsche Bundesbank, and INSEE.

Recommends: pksfc
Homepage: https://github.com/S120/PKSFC
License: Expat
Pkg-Description: R package to simulate Post-Keynesian Stock-Flow Consistent Models
 This package allows to simulate Post-Keynesian Stock-Flow Consistent Models,
 following the approach of Godley, W. and M. Lavoie, 2007: Monetary Economics
 An Integrated Approach to Credit, Money, Income, Production and Wealth.
 .
 The package uses the Gauss-Seidel algorithm to
 solve linear systems of equations, following the approach found in Kinsella,
 Stephen and O’Shea, Terence, Solution and Simulation of Large Stock Flow
 Consistent Monetary Production Models Via the Gauss Seidel Algorithm.

Recommends: repast-symphony
Homepage: https://repast.github.io/repast_simphony.html
License: BSD-3-clause
Pkg-Description: platform for extremely flexible models of interacting agents
 Repast Simphony is a tightly integrated, richly interactive, cross platform
 Java-based modeling system. It supports the development of extremely flexible
 models of interacting agents for use on workstations and computing clusters.
 .
 Repast Simphony models can be developed in several different forms including
 the ReLogo dialect of Logo, point-and-click statecharts, Groovy, or Java, all
 of which can be fluidly interleaved.
 .
 Repast Simphony has been successfully used in many application domains
 including social science, consumer products, supply chains, possible future
 hydrogen infrastructures, and ancient pedestrian traffic to name a few.

Recommends: repast-hpc
Homepage: https://repast.github.io/repast_hpc.html
License: BSD-3-clause
Pkg-Description: agent-based modeling for large-scale distributed computing platforms
 Repast for High Performance Computing (Repast HPC) is a next generation
 agent-based modeling system intended for large-scale distributed computing
 platforms. It implements the core Repast Simphony concepts (e.g. contexts and
 projections), modifying them to work in a parallel distributed environment.
 .
 Repast HPC is written in cross-platform C++. It can be used on workstations,
 clusters, and supercomputers running Apple macOS, Linux, or Unix. Portable
 models can be written in either standard or Logo-style C++.
 .
 Repast HPC has been successfully tested for scalability on Argonne National
 Laboratory's Blue Gene/P.

Recommends: python3-pandasdmx
Homepage: https://pypi.python.org/pypi/pandaSDMX
License: Apache-2.0
Pkg-Description: python- and pandas-powered client for statistical data and metadata exchange
 pandaSDMX is SDMX software that facilitates the acquisition and analysis of
 SDMX-2.1 compliant data and metadata. These can be rendered in various
 formats:
  - as a hierarchical data structure following closely the SDMX 2.1 information
    model. Technically, the model is a layer on top of the XML structure
    rendered by SDMX web services.
  - as arbitrary output generated by dedicated writers. Currently, there is only
    one writer rendering data as pandas Series and DataFrames.

Suggests: elpa-ess

Suggests: dynare-matlab

Suggests: x13as

Suggests: science-mathematics

Suggests: science-numericalcomputation

Suggests: science-statistics

Suggests: science-social

Suggests: science-financial
