Task: debian-science
Relevance: 7
Section: debian-science
Description: Debian Science Pure Blend
 .

Task: science-biology
Parent: debian-science
Section: debian-science
Description: Debian Science Biology packages
 This metapackage will install Debian Science packages related to
 Biology.  You might also be interested in the field::biology debtag.
 .
 This metapackage makes use of the packages med-bio and med-bio-dev
 (for development of biological applications) which are maintained
 by Debian Med - another Debian Pure Blend.  If you are a biologist
 you are most probably interested in the Debian Med project which
 deals with biology and medicine in much more detail then the more
 general Debian Science.
Key:
 science-biology

Task: science-chemistry
Parent: debian-science
Section: debian-science
Description: Debian Science Chemistry packages
 This metapackage will install Debian Science packages related to
 Chemistry.  You might also be interested in the field::chemistry
 debtag and, depending on your focus, in the education-chemistry
 metapackage.
Key:
 science-chemistry

Task: science-dataacquisition
Parent: debian-science
Section: debian-science
Description: Debian Science data acquisition packages
 This metapackage will install Debian Science packages related to data
 acquisition.  This might be used in several sciences (as for instance
 science-viewing and science-typesetting).
Test-new-install: mark show
Key:
 science-dataacquisition

Task: science-dataacquisition-dev
Parent: debian-science
Section: debian-science
Description: Debian Science data acquisition development packages
 This metapackage will install Debian Science packages which are helpful
 to develop applications related to data  acquisition.  This might be
 used in several sciences.
Test-new-install: mark show
Key:
 science-dataacquisition-dev

Task: science-datamanagement
Parent: debian-science
Section: debian-science
Description: Debian Science Data Management packages
 This metapackage will install packages to assist with data management
 tasks, such as obtaining data from remote resources, keeping data
 under version control, etc.
Test-new-install: mark show
Key:
 science-datamanagement

Task: science-distributedcomputing
Parent: debian-science
Section: debian-science
Description: Debian Science Distributed Computing packages
 This metapackage will install Debian Science packages useful for
 various types of distributed computing, such as grid-, cloud-, cluster-
 and parallel-computing.
Test-new-install: mark show
Key:
 science-distributedcomputing

Task: science-economics
Parent: debian-science
Section: debian-science
Description: Debian Science Economics packages
 This metapackage will install Debian Science packages useful for economics and
 econometrics. It includes user-friendly programs for simulating and estimating
 macro-economic and micro-economic models. It also provides computing
 environments which can solve a wide range of problems typically encountered in
 economic research. These environments provide functionalities similar to those
 of popular non-free systems (such as MATLAB, Mathematica, Stata or SAS).
Key:
 science-economics

Task: science-electrophysiology
Parent: debian-science
Section: debian-science
Description: Debian Science packages for Electrophysiology
 This metapackage will install Debian packages which might be useful for
 scientists doing electrophysiology-based neuroscience research.
 .
 The selection of packages is targeting the application of analysis
 techniques. Methods developers are referred to the
 science-statistics, science-imageanalysis, science-numericalcomputation,
 med-imaging, and med-imaging-dev metapackages for a variety of additional
 software that might be useful for electrophysiology research.
Key:
 science-electrophysiology

Task: science-engineering
Parent: debian-science
Section: debian-science
Description: Debian Science Engineering packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Engineering.
 .
 Please note that there is an additional package engineering-dev
 which depends from packages which are useful to develop engineering
 related software.
Key:
 science-engineering

Task: science-engineering-dev
Parent: debian-science
Section: debian-science
Description: Debian Science Engineering-dev packages
 This metapackage will install Debian Science packages which might be
 helpful for development of applications for Engineering.
 .
 You might also be interested in the science-engineering metapackage.
Key:
 science-engineering-dev

Task: science-financial
Parent: debian-science
Section: debian-science
Description: Debian Science financial engineering and computational finance
 This metapackage will install Debian Science packages for financial
 engineering and computational finance.
Key:
 science-financial

Task: science-geography
Parent: debian-science
Section: debian-science
Description: Debian Science Geography packages
 This metapackage will install Debian Science packages related to
 Geography.  You might also be interested in the field::geography
 debtag and, depending on your focus, in the education-geography
 metapackage.
Key:
 science-geography

Task: science-geometry
Parent: debian-science
Section: debian-science
Description: Debian Science geometry packages
 This metapackage will install Debian Science packages related to
 geometry.  You might also be interested in the field::mathematics
 debtag and, depending on your focus, in the education-mathematics
 metapackage.
Key:
 science-geometry

Task: science-highenergy-physics
Parent: debian-science
Section: debian-science
Description: Debian Science High Energy Physics packages
 This metapackage will install Debian Science packages related to High Energy
 Physics, which is a branch of physics that studies the elementary subatomic
 constituents of matter and radiation, and their interactions.  The field is
 also called Particle Physics.
 .
 You might also be interested in the debtag field::physics and, depending on
 your focus, in the physics and education-physics metapackages.
Key:
 science-highenergy-physics

Task: science-highenergy-physics-dev
Parent: debian-science
Section: debian-science
Description: Debian Science High Energy Physics development packages
 This metapackage will install Debian Science packages related to development
 of High Energy Physics applications, which is a branch of physics that studies
 the elementary subatomic constituents of matter and radiation, and their
 interactions.  The field is also called Particle Physics.
 .
 You might also be interested in the debtag field::physics and, depending on
 your focus, in the physics and education-physics metapackages.
Key:
 science-highenergy-physics-dev

Task: science-imageanalysis
Parent: debian-science
Section: debian-science
Description: Debian Science image analysis packages
 This metapackage will install Debian Science packages related to
 scientific image acquisition.  This might be used in several
 sciences (as for instance science-dataacquisition, science-viewing
 and science-typesetting).
Test-new-install: mark show
Key:
 science-imageanalysis

Task: science-imageanalysis-dev
Parent: debian-science
Section: debian-science
Description: Debian Science development of image analysis applications
 This metapackage will install Debian Science development libraries to
 develop scientific image analysis applications acquisitions.
Test-new-install: mark show
Key:
 science-imageanalysis-dev

Task: science-linguistics
Parent: debian-science
Section: debian-science
Description: Debian Science Linguistics packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Linguistics.
Key:
 science-linguistics

Task: science-logic
Parent: debian-science
Section: debian-science
Description: Debian Science Logic packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Computational Logic.  It contains
 formula transformation tools, solvers for formulas specified in
 various logics, interactive proof systems, etc.
Key:
 science-logic

Task: science-machine-learning
Parent: debian-science
Section: debian-science
Description: Debian Science Machine Learning packages
 This metapackage will install packages useful for machine learning.
 Included packages range from knowledge-based (expert) inference
 systems to software implementing the advanced statistical methods
 that currently dominate the field.
Test-new-install: mark show
Key:
 science-machine-learning

Task: science-mathematics
Parent: debian-science
Section: debian-science
Description: Debian Science Mathematics packages
 This metapackage will install Debian Science packages related to
 Mathematics.  You might also be interested in the field::mathematics
 debtag and, depending on your focus, in the education-mathematics
 metapackage.
Key:
 science-mathematics

Task: science-mathematics-dev
Parent: debian-science
Section: debian-science
Description: Debian Science Mathematics-dev packages
 This metapackage will install Debian Science packages which might be
 helpful for development of applications for Mathematics.
 .
 You might also be interested in the science-mathematics metapackage.
Key:
 science-mathematics-dev

Task: science-meteorology
Parent: debian-science
Section: debian-science
Description: Debian Science Meteorology packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Meteorology and Climate.
Key:
 science-meteorology

Task: science-meteorology-dev
Parent: debian-science
Section: debian-science
Description: Debian Science Meteorology-dev packages
 This metapackage will install Debian Science packages which might be
 helpful for development of applications for Meteorology and Climate.
 .
 You might also be interested in the science-meteorology metapackage.
Key:
 science-meteorology-dev

Task: science-nanoscale-physics
Parent: debian-science
Section: debian-science
Description: Debian Science Nanoscale Physics packages
 This metapackage will install Debian Science packages related to
 Nanoscale Physics, which corresponds to the study of physical systems
 typically ranging from 1 to 100 nm in size. The properties of such
 systems usually depend on the number of atoms they are made of, while
 this number is still relatively large for an accurate description.
 .
 The nanoscale is the meeting point of classical and quantum physics.
 Previous research efforts were considering either smaller systems, for
 which everybody could develop their own methods and software
 independently, or much bigger systems, for which it was clearly
 impossible to provide a fine-grained description. Addressing the issues
 raised by the nanoscale requires however cooperative and coordinated
 efforts in a multidisciplinary context. This metapackage is part of
 such an endeavor.
 .
 You might also be interested in the debtag field::physics and, depending on
 your focus, in the physics and education-physics metapackages.
Key:
 science-nanoscale-physics

Task: science-nanoscale-physics-dev
Parent: debian-science
Section: debian-science
Description: Debian Science Nanoscale Physics development packages
 This metapackage will install Debian Science packages which might be
 helpful for the development of applications for Nanoscale Physics.
 .
 You might also be interested in the debtag field::physics and, depending
 on your focus, in the nanoscale-physics, physics and education-physics
 metapackages.
Key:
 science-nanoscale-physics-dev

Task: science-neuroscience-cognitive
Parent: debian-science
Section: debian-science
Description: Debian Science packages for Cognitive Neuroscience
 This metapackage will install Debian packages which might be useful for
 scientists doing cognitive neuroscience research. This comprises the full
 research process from conducting psychophysical experiments, over data
 acquisition and analysis, to visualization and typesetting of scientific
 results.
 .
 The selection of packages is targeting the application of analysis
 techniques. Methods developers are referred to the science-statistics,
 science-imageanalysis, science-numericalcomputation, med-imaging, and
 med-imaging-dev metapackages for a variety of additional software that
 might be useful in the context of cognitive neuroscience.
Key:
 science-neuroscience-cognitive

Task: science-neuroscience-modeling
Parent: debian-science
Section: debian-science
Description: Debian Science packages for modeling of neural systems
 This metapackage will install Debian packages which might be useful for
 scientists interested in modeling of real neural systems at different
 levels (from single neuron to complex networks).
 .
 The selection of packages is targeting the application of simulation
 techniques. Methods developers are referred to the
 science-statistics, science-imageanalysis,
 science-numericalcomputation, med-imaging, and med-imaging-dev
 metapackages for a variety of additional software that might be
 useful for neuroscience research.
Key:
 science-neuroscience-modeling

Task: science-numericalcomputation
Parent: debian-science
Section: debian-science
Description: Debian Science Numerical Computation packages
 This metapackage will install Debian Science packages useful for
 numerical computation. The packages provide an array oriented
 calculation and visualisation system for scientific computing and
 data analysis. These packages are similar to commercial systems such
 as Matlab and IDL.
Test-new-install: mark show
Key:
 science-numericalcomputation

Task: science-physics
Parent: debian-science
Section: debian-science
Description: Debian Science Physics packages
 This metapackage will install Debian Science packages related to Physics.
 You might also be interested in the debtag field::physics and, depending on
 your focus, in education-physics metapackage.
Key:
 science-physics

Task: science-physics-dev
Parent: debian-science
Section: debian-science
Description: Debian Science Physics-dev packages
 This metapackage will install Debian Science packages which might be
 helpful for development of applications for Mathematics.
 .
 You might also be interested in the science-physics metapackage.
Key:
 science-physics-dev

Task: science-presentation
Parent: debian-science
Section: debian-science
Description: Debian Science generic tools for presentations
 This metapackage will install Debian Science some packages which are
 useful for doing presentations for instance on scientific conferences.
Test-new-install: mark show
Key:
 science-presentation

Task: science-psychophysics
Parent: debian-science
Section: debian-science
Description: Debian Science packages for Psychophysics
 This metapackage will install Debian packages which might be useful for
 carrying out any experiment relating physical stimuli and their
 psychological effects.
 .
 The selection of packages is targeting software for stimuli delivery.
 For additional software related to the analysis of the acquired data
 refer to science-neuroscience-cognitive, med-imaging depending on the
 domain of application.  Additionally look into
 science-bci since those often provide a complete loop
 frameworks including stimuli delivery.
Key:
 science-psychophysics

Task: science-robotics
Parent: debian-science
Section: debian-science
Description: Debian Robotics packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Robotics.
 .
 You might also be interested in the science-engineering metapackage.
Key:
 science-robotics

Task: science-robotics-dev
Parent: debian-science
Section: debian-science
Description: Debian Robotics development packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages that are relevant to develop applications
 for robotics.
Key:
 science-robotics-dev

Task: science-simulations
Parent: debian-science
Section: debian-science
Description: Debian Science Simulation packages
 This metapackage will install Debian Science packages that are
 used to do simulations in different fields of science.
Test-new-install: mark show
Key:
 science-simulations

Task: science-statistics
Parent: debian-science
Section: debian-science
Description: Debian Science Statistics packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to statistics.  This task is a general
 task which might be useful for any scientific work.  It depends from
 a lot of R packages as well as from other tools which are useful to
 do statistics.  Moreover the Science Mathematics task is suggested
 to optionally install all mathematics related software.
Test-new-install: mark show
Key:
 science-statistics

Task: science-typesetting
Parent: debian-science
Section: debian-science
Description: Debian Science typesetting packages
 This metapackage will install Debian Science packages related to
 typesetting.  You might also be interested in the use::typesetting
 debtag.
Test-new-install: mark show
Key:
 science-typesetting

Task: science-viewing
Parent: debian-science
Section: debian-science
Description: Debian Science data visualisation packages
 This metapackage will install Debian Science packages related to data
 visualization. You might also be interested in the use::viewing
 debtag.
Test-new-install: mark show
Key:
 science-viewing

Task: science-viewing-dev
Parent: debian-science
Section: debian-science
Description: Debian Science development of visualisation applications
 This metapackage will install Debian Science packages related to the
 development of applications to visualise scientific data.
Test-new-install: mark show
Key:
 science-viewing-dev

Task: science-workflow
Parent: debian-science
Section: debian-science
Description: workflow management systems useful for scientific research
 This task lists some packages providing workflow management
 systems useful for scientific research.
Test-new-install: mark show
Key:
 science-workflow

